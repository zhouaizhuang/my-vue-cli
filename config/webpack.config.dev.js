const path = require('path') // 引入node内置模块path
const HtmlWebpackPlugin = require('html-webpack-plugin') // 构建html文件
const VueLoaderPlugin = require('vue-loader/lib/plugin') // vue-loader编译vue文件
// const { CleanWebpackPlugin } = require('clean-webpack-plugin') // 清理构建目录下的文件
const ProgressBarWebpackPlugin = require('progress-bar-webpack-plugin') // 设置打包进度条
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin') // 优化提示
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin') // webpack5 改为此插件 css样式压缩
const Dotenv = require('dotenv-webpack') // 支持程序获取.env配置的环境变量
const getStyleloaders = pre => [ 'vue-style-loader', "css-loader", 'postcss-loader', pre].filter(Boolean)
module.exports = {
  entry: './src/main.js', // 入口文件，把src下的main.js编译到出口文件
  output: { // 出口文件
    path: undefined, // 打包后文件夹名字，这里是开发环境所以是undefined
    filename: 'static/js/[name].js', // 输出到哪儿
    chunkFilename: 'static/js/[name].chunk.js', // 名称是什么
    assetModuleFilename: 'static/media/[hash:10]ext[query]', // 静态资源名称
    clean: true, // 打包前先删除旧的dist文件夹
  },
  module: {
    rules: [
      {test: /\.css$/,use: getStyleloaders()},
      {test: /.less$/,use: getStyleloaders('less-loader')},
      {test: /\.(png|jpe?g|gif|webp|svg)$/, type: "asset", parser: {dataUrlCondition:{ maxSize: 10 * 1024 } },generator: {filename: "static/images/[hash:10][ext][query]"}},
      {test: /\.(ttf|woff2?|mp3|mp4|avi)$/, type: 'asset/resource', generator: {filename: "static/media/[hash:10][ext][query]"}},
      {test: /\.(ts|js)x?$/,exclude: /node_modules/,use: ['babel-loader']},
      // {test: /\.(ts|js)x?$/,exclude: /node_modules/,loader:"./config/loaders/test-loader"},
      // {test: /\.(ts|js)x?$/,exclude: /node_modules/,loader:"./config/loaders/banner-loader", options: { author: "老张"}},
      // {test: /\.(ts|js)x?$/,exclude: /node_modules/,loader:"./config/loaders/babel-loader", options: { presets: ["@babel/preset-env"]}},
      // {test: /\.(png|jpe?g|gif|webp|svg)$/, loader: './config/loaders/file-loader', type: 'javascript/auto'}, // 阻止webpack默认处理file资源
      {test: /\.vue$/,loader: 'vue-loader'},
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ // 自动插入到dist目录中
      title: 'webpack搭建vue项目',
      template: path.resolve(__dirname, "../public/index.html"),
    }),
    new VueLoaderPlugin(),
    // new CleanWebpackPlugin(),
    new ProgressBarWebpackPlugin({complete: '█',clear: true,}),
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: { messages: ['You can now view project-vue in the browser.'] }, // 成功的时候输出
      clearConsole: true, // 是否每次都清空控制台
    }),
    new CssMinimizerPlugin(),
    new Dotenv({path: path.resolve(__dirname, `../.env.${process.env.NODE_ENV}`)}),
  ].filter(Boolean),
  stats: 'errors-only', // 日志打印只打印错误和警告
  optimization: { nodeEnv: false },
  resolve: {
    extensions: ['.js', '.vue', '.json'], // 引入路径是不用写对应的后缀名
    alias: {
      vue$: 'vue/dist/vue.esm.js', // 正在使用的是vue的运行时版本，而此版本中的编译器时不可用的，我们需要把它切换成运行时 + 编译的版本
      '@': path.resolve(__dirname, '../src'), // 用@直接指引到src目录下
      '@assets': path.resolve(__dirname, '../src/assets'), // 用@直接指引到src/assets目录下
    },
  },
  devServer: {
    open: false, // 自动打开浏览器
    host: "localhost",
    hot: true, // 热更新打开
    port: '8888', // 端口：8888
    historyApiFallback: true, // 解决前端路由刷新404问题
  },
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development', // 开发模式
  devtool: 'cheap-module-source-map',
}