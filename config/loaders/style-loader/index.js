module.exports = function(content){
  
}
module.exports.pitch = function(remainingRequest){
  // remainingRequest 剩下还需要处理的loader
  console.log(remainingRequest)
  // 1、将remainingRequest中的绝对路径改为相对路径
  const relativePath = remainingRequest.split('!').map(absolutePath => this.utils.contextify(this.context, absolutePath)).join('!')
  const script = `
    import style from "!!${relativePath}"
    const styleE1 = document.createElement('style')
    styleE1.innerHTML = style
    document.head.appendChild(styleE1) 
  `
  return script
}