/**
 * 清楚项目中的console.log
 * @param {*} content 接受到的文件的内容
 * @param {*} map SourceMap
 * @param {*} meta 别的loader传过来的数据
 * @returns 返回处理之后的内容
 */
module.exports = function (content, map, meta){
  return content.replace(/console.log\(.*\);?/g, '')
}