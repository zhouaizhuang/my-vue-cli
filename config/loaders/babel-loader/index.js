const babel = require('@babel/core')
const schema = require("./schema.json")
module.exports = function (content){
  const options = this.getOptions(schema)
  /**使用同步转换babel对代码进行编译转换*/
  const result = babel.transformSync(content, {...options, configFile: false});
  return result.code
  /**使用异步转换babel */
  // const callback = this.async()
  // babel.transform(content, {...options, configFile: false}, function(err, result){
  //   if(err) { callback(err) }
  //   callback(null, result.code)
  // })
}