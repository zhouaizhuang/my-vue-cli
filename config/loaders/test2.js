/**
 * 异步loader
 * @param {*} content 接受到的文件的内容
 * @param {*} map SourceMap
 * @param {*} meta 别的loader传过来的数据
 * @returns 返回处理之后的内容
 */
module.exports = function (content, map, meta){
  const callback = this.async()
  setTimeout(() => {
    console.log('test2')
    callback(null, content, map, meta)
  }, 1000)
}