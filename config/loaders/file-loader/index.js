module.exports = function (content){
  // 1、根据文件内容生成hash值文件名
  const interpolatedName = loaderUtils.interpolateName(this, "[hash].[ext].[query]", {
    content
  })
  // console.log(interpolatedName)
  interpolatedName = `image/${interpolatedName}`
  // 2、将文件输出出去
  this.$emitFile(interpolatedName, content)
  // 3、返回module.exports = "文件路径（文件名）"
  return `module.exports = "${interpolatedName}"`
}
// 需要处理图片、字体等文件，他们都是buffer数据
module.exports.raw = true