/**
 * pitchloader
 * @param {*} content 接受到的内容为buffer数据。用于处理js、css之外的一些媒体资源
 * @param {*} map SourceMap
 * @param {*} meta 别的loader传过来的数据
 * @returns 返回处理之后的内容
 */
module.exports = function (content, map, meta){
  return content
}
module.exports.pitch = function (){
  console.log('在执行loader之前，我就执行') // 多个pitch loader的话。会先从左向右先执行完pitch方法，然后从右向左执行loader方法
  return 'result' // 如果这个pitch return了，那么直接放弃之后的pitch、loader。直接跳转到之前的已经执行过pitch的loader。（而且当前pitch对应的loader也不执行了）
}