/**
 * rawloader
 * @param {*} content 接受到的内容为buffer数据。用于处理js、css之外的一些媒体资源
 * @param {*} map SourceMap
 * @param {*} meta 别的loader传过来的数据
 * @returns 返回处理之后的内容
 */
module.exports = function (content, map, meta){
  return content
}
module.exports.raw = true