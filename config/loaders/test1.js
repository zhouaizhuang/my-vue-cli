/**
 * 同步loader
 * @param {*} content 接受到的文件的内容
 * @param {*} map SourceMap
 * @param {*} meta 别的loader传过来的数据
 * @returns 返回处理之后的内容
 */
module.exports = function (content, map, meta){
  /**
   * 第一个参数 err代表是否有错误
   * 第二个参数 content处理之后的内容
   * 第三个参数 source-map继续传递source-map
   * 第四个参数 meta给下一个loader传递参数
   */
  console.log('test1')
  this.callback(null, content, map, meta)
}