import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { go, goBack } from "@/router/tools.js"
import * as Z from "./common.js"
import "./common.css"
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(ViewUI) // 全局加载
Vue.config.productionTip = false
console.log('123123===')
// 环境变量
console.log(process.env.NODE_ENV)
window._ = Z
window._.go = go
window._.goBack = goBack
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
