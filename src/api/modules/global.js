import { post, mock } from "@/utils/network.js"
export const getAccountMenu = function (params = {}) {
  const mockData = {
    list: [
      {
        id: "1",
        name: "/home",
        title: '工作台',
        icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/d59b91854ca2416eaddb76d2324c61a6.png',
        activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3f55033123524e419d6d7f2dfaa26289.png',
        children: [
          {
            id: "10",
            name: "/home/index", 
            title: '数据看板',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [
              {
                id: "100",
                name: "/home/index/index",
                title: '首页',
                icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
                activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
                children: []
              },
              {
                id: "101",
                name: "/home/index/detail",
                title: '详情',
                icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
                activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
                children: []
              }
            ],
          },
          {
            id: "11",
            name: "/home/calc", 
            title: '数据统计',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [
              {
                id: "110",
                name: "/home/calc/index",
                title: '公司报表',
                icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
                activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
                children: []
              }
            ],
          },
        ],
      },
      {
        id: "2",
        name: "/healthCheck",
        title: '健康管理',
        icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/d59b91854ca2416eaddb76d2324c61a6.png',
        activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3f55033123524e419d6d7f2dfaa26289.png',
        children: [
          {
            id: "20",
            name: "/healthCheck/index", 
            title: '健康检测',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          }
        ],
      },
      {
        id: "3",
        name: "/healthFile",
        title: '档案管理',
        icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/d59b91854ca2416eaddb76d2324c61a6.png',
        activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3f55033123524e419d6d7f2dfaa26289.png',
        children: [
          {
            id: "30",
            name: "/healthFile/index", 
            title: '健康档案',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          }
        ],
      },
      {
        id: "4",
        name: "/set",
        title: '设置',
        icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/d59b91854ca2416eaddb76d2324c61a6.png',
        activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3f55033123524e419d6d7f2dfaa26289.png',
        children: [
          {
            id: "40",
            name: "/set/permission", 
            title: '权限管理',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          },
          {
            id: "41",
            name: "/set/user", 
            title: '用户管理',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          },
          {
            id: "42",
            name: "/set/third", 
            title: '三方平台设置',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          }
        ],
      },
      {
        id: "4",
        name: "/order",
        title: '订单',
        icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/d59b91854ca2416eaddb76d2324c61a6.png',
        activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3f55033123524e419d6d7f2dfaa26289.png',
        children: [
          {
            id: "40",
            name: "/order/goodList", 
            title: '商品列表',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          },
          {
            id: "41",
            name: "/order/reportList", 
            title: '报告列表',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          },
          {
            id: "41",
            name: "/order/productList", 
            title: '产品列表',
            icon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/1e37614a9c794e2693985f7487a1ddb5.png',
            activeIcon: 'http://218.93.39.18:18391/robot/accessPictures?fileName=upload/image/2023/04/09/3d77fce00550452aa23ffc34db363923.png',
            children: [],
          }
        ],
      },
    ],
  } 
  return mock('/api/getAccountMenu', params, mockData)
}