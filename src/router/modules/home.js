import layout from '@/views/layout/index.vue'
export default [
  {
    path: '/home',
    name: '/home',
    meta: { title: '工作台', sort: 1 },
    component: layout,
    redirect: '/home/index',
    children: [
      {
        path: 'index',
        name: '/home/index',
        meta: { title: '数据看板', token: false },
        component: () => import(/* webpackChunkName: "home" */'../../views/home/index/index.vue'),
        redirect: '/home/index/index',
        children: [
          {
            path: 'index',
            name: '/home/index/index',
            meta: { title: '首页', token: false },
            component: () => import(/* webpackChunkName: "home" */'../../views/home/index/index.vue'),
          },
          {
            path: 'detail',
            name: '/home/index/detail',
            meta: { title: '详情', token: false },
            component: () => import(/* webpackChunkName: "home" */'../../views/home/index/index.vue'),
          },
        ]
      },
      {
        path: 'calc',
        name: '/home/calc',
        meta: { title: '数据统计', token: false },
        component: () => import(/* webpackChunkName: "home" */'../../views/home/index/index.vue'),
        redirect: '/home/calc/index',
        children: [
          {
            path: 'index',
            name: '/home/calc/index',
            meta: { title: '公司报表', token: false },
            component: () => import(/* webpackChunkName: "home" */'../../views/home/index/index.vue'),
          }
        ]
      },
    ]
  },
]