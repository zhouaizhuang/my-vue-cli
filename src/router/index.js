import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index.js'
import { startLoadAni, isLogin } from "./tools.js"
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({ showSpinner: false }) // 注意nprogress的导航条配色是在custom.css中配置的#nprogress .bar{}
Vue.use(VueRouter)
// 1、本地全部动态路由列表，通过权限控制
const r = require.context('./modules', true, /\.js/)
export const localRouteList = r.keys().reduce((prev, item) => [...prev, ...Array.isArray(r(item).default) ? r(item).default : [r(item).default]], [])
// 2、每个角色都需要的路由，不通过权限管理
export const frontEndRouteList = [
]
// 3、静态路由列表，静默注册(不显示在菜单栏)-------登录相关
const routes = [
  { path: '/', meta: {}, redirect: { path: '/home' } },
]
const router = new VueRouter({
  routes,
  scrollBehavior: (to, from, savePosition) => savePosition || { x: 0, y: 0 } // 在点击浏览器的“前进/后退”，或者切换导航的时候触发。
})
// 路由（页面级别权限控制）（只要meta标签中needLogin设置为true。同时token不存在则会回到登录页面）
router.beforeEach(async (to, from, next) => {
  NProgress.start() // 这是为了解决首次加载白屏之后的页面加载等待时间
  _.setLocalStorage('token', '100.b803ee9c4fb642f18a458b34bf700d29')
  _.setLocalStorage('userId', '1')
  startLoadAni() // 开启动画
  _.setTitle(to.meta.title || '') // 设置网页标题
  if(isLogin()) {
    let dynamicRouteList = store.state.dynamicRouteList || [] // 全局动态路由列表
    // 这里默认是必须保留一个权限的，如果有全部权限都被清除的情况。那就需要改造成，变量存储是否首次触发。只有首次可以执行下去。
    if(!store.state.isLoadRoute) {
      dynamicRouteList = await store.dispatch('getRouteAsync') // 拿到比对后生成的当前用户权限有效的路由
      // 路由动态注入------->   location.reload()可以清空动态注入的路由。注意在退出登录的时候需要做
      const baseLen = [...routes, ...frontEndRouteList].length // 未获取后台路由时候，基础的路由长度
      if(router.options.routes.length <= baseLen) {
        dynamicRouteList.forEach(item => { router.options.routes.push(item);router.addRoute(item) })
      }
      next({ ...to })
    } else {
      if(to.name) {
        let tabList = to.name == 'login' ? [] : _.radioChecked(_.uniqueObj([...store.state.tabList, to], 'name'), {name: to.name ||''})
        store.commit('setTabList', tabList)
      }
      next()
    }
  } else {
    !_.safeGet(() => to.meta.token, true) || to.path == '/login' ? next() : next('/login')
  }
})
// 路由后置守卫
router.afterEach(() => {
  NProgress.done()
})
export default router
