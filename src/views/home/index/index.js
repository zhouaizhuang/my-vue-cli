import * as func from "./func.js"
import { randomColor } from "@/common.js"
export default {
  name: '',
  components:{
    
  },
  data(){
    return {
      list: new Array(20).fill({}).map(v => ({...v, bgColor: randomColor()}))
    }
  },
  methods:{
    ...func,
  },
  created(){
  },
  mounted(){
  },
}