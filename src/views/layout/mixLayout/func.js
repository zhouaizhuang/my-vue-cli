import { range, getViewPos, searchCover } from "@/common.js"
import { leftConfig, tabConfig } from "@/config/mixLayout.config.js"
import { go } from "@/router/index.js"
// 调整收起和展开状态 
export const toogleSpread = function () {
  leftConfig.isMini = !leftConfig.isMini
}
// 选择条目触发选中
export const selectFirstTab = function (ref, item) {
  this.selectTab = ref
  const father = this.$refs['first-menu-box'].getBoundingClientRect()
  const child = this.$refs[ref][0].getBoundingClientRect()
  this.posLeft = child.left - father.left + this.moveX * -1
  this.tabWidth = child.width
  this.secondTabList = item.children
  _.go(item)
}
// 计算以及标题渲染后的最大宽度
export const clacMaxWidth = function () {
  const {left: fatherLeft} = getViewPos(this.$refs['first-menu-box'])
  const {right} = getViewPos(this.$refs[`tab-${this.$store.state.dynamicRouteList.length-1}`][0])
  const maxWidth = right - fatherLeft - this.moveX
  return maxWidth
}
// 前往上一页
export const goPrev = function () {
  const maxWidth = this.clacMaxWidth()
  this.moveX = range(this.moveX + this.divide, -1 * maxWidth + 200, 0)
}
// 下一页
export const goNext = function () {
  const child = getViewPos(this.$refs[`tab-${this.$store.state.dynamicRouteList.length-1}`][0])
  const father = getViewPos(this.$refs['first-menu-box'])
  if(child.right > father.right) {
    this.moveX = this.moveX - this.divide
  }
}
// 计算tabs最大长度
export const clacMaxTabWidth = function () {
  const { left: fatherLeft } = getViewPos(this.$refs['father-tab-box'])
  const { right } = getViewPos(this.$refs[`tabRef${this.$store.state.tabList.length - 1}`][0])
  const maxWidth = right - fatherLeft - this.tabMoveX
  return maxWidth
}
// 小标签上一页
export const goTabPrev = function () {
  const maxWidth = this.clacMaxTabWidth()
  this.tabMoveX = range(this.tabMoveX + this.divide, -1 * maxWidth + 200, 0)
}
// 小标签下一页
export const goTabNext = function () {
  const child = getViewPos(this.$refs[`tabRef${this.$store.state.tabList.length-1}`][0])
  const father = getViewPos(this.$refs['father-tab-box'])
  if(child.right > father.right) {
    this.tabMoveX = this.tabMoveX - this.divide
  }
}
// 处理以及菜单
export const processFirstMenu = function () {
  this.$nextTick(() => {
    const {width} = this.$refs['first-menu-box'].getBoundingClientRect()
    const maxWidth = this.clacMaxWidth()
    this.isShowArrow = maxWidth > width
    this.moveX = this.isShowArrow ? this.moveX : 0
  })
}
// 处理tabs
export const processTabs = function () {
  if(!tabConfig.isShow) { return false }
  this.$nextTick(() => {
    const { left: fatherLeft, width: tabWidth } = getViewPos(this.$refs['father-tab-box'])
    const { right } = getViewPos(this.$refs[`tabRef${this.$store.state.tabList.length - 1}`][0])
    const maxWidth = right - fatherLeft - this.tabMoveX
    this.isShowTabArrow = maxWidth > tabWidth
    this.tabMoveX = this.isShowTabArrow ? this.tabMoveX : 0
  })
}
// 检查是否显示出箭头
export const checkArrow = function () {
  this.processFirstMenu() // 处理一级菜单
  this.processTabs() // 处理tabs
}
// 关闭当前标签
export const closeCurTab = function (item) {
  let tabList = this.$store.state.tabList
  const index = tabList.findIndex(v => v.name == item.name)
  tabList = tabList.filter((v, i) => i !== index)
  this.$store.commit('setTabList', tabList)
  this.processTabs() // 只能计算是否显示左右箭头
  let selectItem = tabList.find(v => v.isChecked)
  if(!selectItem) {
    const checkedIndex = Math.min(tabList.length - 1, index) // 获取下一个选中的tab
    _.go(tabList[checkedIndex])
  }
}
// 选择当前tab
export const selectCurItem = function (item) {
  _.go(item)
}
// 选中二级菜单
export const selectSecondItem = function (item) {
  // if(!item.children.length) { return _.go(item) }
  this.secondTabList = searchCover(this.secondTabList, {name: item.name}, v => ({...v, isSpread: !v.isSpread}))
  // if(!item.isSpread) {
    _.go(item)
  // }
}
// 点击三级菜单
export const selectThirdItem = function (item) {
  _.go(item)
}
// 点击icon1
export const clickIcon1 = function () {
  console.log('点击icon1')
}
// 点击icon2
export const clickIcon2 = function () {
  console.log('点击icon2')
}