import * as func from "./func.js"
import { loadAni, topConfig, leftConfig, tabConfig } from "@/config/mixLayout.config.js"
import { safeGet, getViewPos } from "@/common.js" 
import HighImg from "@/components/HighImg/index.vue"
export default {
  name: '',
  components:{
    HighImg
  },
  data(){
    return {
      loadAni,
      topConfig,
      leftConfig,
      tabConfig,
      posLeft: topConfig.firstMenu.marginLeft, // 偏移量left值
      tabWidth: 50, // 一级标题宽度
      selectTab: 'tab-0', // 当前选中的以及标题
      secondTabList: [],
      moveX: 0, // 菜单栏移动距离
      divide: 300, // 菜单栏每次前进和后退的间隔
      timer:null,
      isShowArrow: false, // 是否显示一级标题的箭头
      isShowTabArrow: false, // 是否显示tab的小箭头
      tabMoveX:0, // tab移动距离
    }
  },
  watch:{
    '$route': {
      handler(newVal, oldVal) {
        this.processTabs()
      },
      deep: true
    }
  },
  methods:{
    ...func,
  },
  created(){
    this.secondTabList = safeGet(() => this.$store.state.dynamicRouteList[0].children, [])
    window.onresize = async () =>{
      clearTimeout(this.timer)
      this.timer = setTimeout(() => this.checkArrow(), 600)
    }
  },
  async mounted(){
    this.checkArrow() // 此处应该是等待接口拿到值之后，在dom挂载之后，再执行
    this.tabWidth = safeGet(() => getViewPos(this.$refs['tab-0'][0]).width, 55)
  },
}