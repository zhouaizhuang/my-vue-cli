import { range, getViewPos, searchCover } from "@/common.js"
import { go } from "@/router/index.js"
// 调整收起和展开状态 
export const toogleSpread = function () {
  this.leftConfig.isMini = !this.leftConfig.isMini
}
// 前往上一页
export const goPrev = function () {
  const maxWidth = this.clacMaxWidth()
  this.moveX = range(this.moveX + this.divide, -1 * maxWidth + 200, 0)
}
// 计算tabs最大长度
export const clacMaxTabWidth = function () {
  const { left: fatherLeft } = getViewPos(this.$refs['father-tab-box'])
  const { right } = getViewPos(this.$refs[`tabRef${this.$store.state.tabList.length - 1}`][0])
  const maxWidth = right - fatherLeft - this.tabMoveX
  return maxWidth
}
// 下一页
export const goNext = function () {
  const child = getViewPos(this.$refs[`tab-${this.routeList.length-1}`][0])
  const father = getViewPos(this.$refs['first-menu-box'])
  if(child.right > father.right) {
    this.moveX = this.moveX - this.divide
  }
}
// 小标签上一页
export const goTabPrev = function () {
  const maxWidth = this.clacMaxTabWidth()
  this.tabMoveX = range(this.tabMoveX + this.divide, -1 * maxWidth + 200, 0)
}
// 小标签下一页
export const goTabNext = function () {
  const child = getViewPos(this.$refs[`tabRef${this.$store.state.tabList.length-1}`][0])
  const father = getViewPos(this.$refs['father-tab-box'])
  if(child.right > father.right) {
    this.tabMoveX = this.tabMoveX - this.divide
  }
}
// 处理tabs
export const processTabs = function () {
  if(!this.tabConfig.isShow) { return false }
  this.$nextTick(() => {
    const { left: fatherLeft, width: tabWidth } = getViewPos(this.$refs['father-tab-box'])
    const { right } = getViewPos(this.$refs[`tabRef${this.$store.state.tabList.length - 1}`][0])
    const maxWidth = right - fatherLeft - this.tabMoveX
    this.isShowTabArrow = maxWidth > tabWidth
    this.tabMoveX = this.isShowTabArrow ? this.tabMoveX : 0
  })
}
// 选择当前tab
export const selectCurItem = function (item) {
  _.go(item)
}
// 关闭当前标签
export const closeCurTab = function (item) {
  let tabList = this.$store.state.tabList
  const index = tabList.findIndex(v => v.name == item.name)
  tabList = tabList.filter((v, i) => i !== index)
  this.$store.commit('setTabList', tabList)
  this.processTabs() // 只能计算是否显示左右箭头
  let selectItem = tabList.find(v => v.isChecked)
  if(!selectItem) {
    const checkedIndex = Math.min(tabList.length - 1, index) // 获取下一个选中的tab
    _.go(tabList[checkedIndex])
  }
}
// 选中一级菜单
export const selectFirstItem = function (item) {
  let dynamicRouteList = this.$store.state.dynamicRouteList
  dynamicRouteList = searchCover(dynamicRouteList, {name: item.name}, v => ({...v, isSpread: !v.isSpread}))
  this.$store.commit('setDynamicRouteList', dynamicRouteList)
  if(item.children.length == 0) {
    _.go(item)
  }
}
// 点击二级菜单
export const selectSecondItem = function (item) {
  let dynamicRouteList = this.$store.state.dynamicRouteList
  dynamicRouteList = dynamicRouteList.map(v => {
    v.children = searchCover(v.children, {name: item.name}, v => ({...v, isSpread: !v.isSpread}))
    return v
  })
  this.$store.commit('setDynamicRouteList', dynamicRouteList)
  if(item.children.length == 0) {
    _.go(item)
  }
}
// 点击三级菜单
export const selectThirdItem = function (item) {
  _.go(item)
}
// 点击图标1
export const clickIcon1 = function () {
  
}
// 点击图标2
export const clickIcon2 = function () {
  
}