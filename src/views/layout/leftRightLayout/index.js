import * as func from "./func.js"
import * as leftRightLayout from "@/config/leftRightLayout.config.js"
import { leftConfig, topConfig, tabConfig } from "@/config/leftRightLayout.config.js"
import HighImg from "@/components/HighImg/index.vue"
export default {
  name: '',
  components:{
    HighImg
  },
  data(){
    return {
      leftConfig,
      topConfig,
      tabConfig,
      leftRightLayout,
      posLeft: 0, // 偏移量left值
      tabWidth: 65, // 一级标题宽度
      selectTab: 'tab-0', // 当前选中的以及标题
      breadcrumbList: [],
      moveX: 0, // 菜单栏移动距离
      divide: 500, // 菜单栏每次前进和后退的间隔
      timer:null,
      isShowArrow: false, // 是否显示一级标题的箭头
      isShowTabArrow: false, // 是否显示tab的小箭头
      tabMoveX:0, // tab移动距离
    }
  },
  watch: {
    $route: {
      handler: function (newVal, oldVal){
        const path = newVal.path
        const [firstPath, secondPath, thirdPath] = path.slice(1).split('/')
        let dynamicRouteList = this.$store.state.dynamicRouteList
        this.breadcrumbList = dynamicRouteList.reduce((prev, item)=> {
          if(firstPath == item.path.slice(1)) {
            prev.push({name: item.name, title: item.meta.title})
            const secondItem = item.children.find(k => k.path == secondPath) || {}
            if(Object.keys(secondItem).length && secondPath) {
              prev.push({name: secondItem.name, title: secondItem.meta.title})
              const thirdItem = secondItem.children.find(c => c.path == thirdPath) || {}
              if(Object.keys(thirdItem).length && thirdPath) {
                prev.push({name: thirdItem.name, title: thirdItem.meta.title})
              }
            }
          }
          return prev
        }, [])
        this.processTabs()
      },
      deep: true,
      immediate: true,
    }
  },
  methods:{
    ...func,
  },
  created(){
    window.onresize = async () =>{
      clearTimeout(this.timer)
      this.timer = setTimeout(() => this.processTabs(), 600)
    }
  },
  async mounted(){
    this.processTabs()
  },
}