import {setLocalStorage, setSessionStorage} from "@/common.js"
import { go } from "@/router/index.js"
// 登录
export const login = function () {
  if(!this.userName) {return this.$Message.error('请输入用户名')}
  if(!this.passWord) {return this.$Message.error('请输入密码')}
  this.isShowSlider = true
}
export const handleKeyUp = function(){
  this.login()
}
// 成功的回调
export const success = async function () {
  console.log('成功的回调')
  // const res = await this.$post('/user/login',{account:this.userName, password:this.passWord})
  // if(res) {
  //   const { token, userId } = res
  //   setSessionStorage('token', token)
  //   setSessionStorage('userId', userId)
  // }
  _.go({name: '/home/index/index'})
}