import * as func from "./func.js"
import Slider from "@/components/Slider/index.vue"
export default {
  name: '',
  components:{
    Slider
  },
  data(){
    return {
      userName:'',
      passWord:'',
      isShowSlider: false,
    }
  },
  methods:{
    ...func,
  },
  created(){
    
  },
  mounted(){
    
  },
}