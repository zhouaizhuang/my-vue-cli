import QRCode from "qrcodejs2"
export default {
  name: 'QRcode',
  props: {
    content: { // 二维码内容
      type: String,
      default: 'test'
    },
    colorDark: { // 二维码背景色
      type: String,
      default: '#000'
    },
    colorLight: { // 二维码颜色
      type: String,
      default: '#fff'
    },
    width:{ // 二维码宽度
      type: String,
      default: "100"
    },
    height: { // 二维码高度
      type: String,
      default: "100"
    }
  },
  components:{},
  data(){
    return {
    }
  },
  methods:{
  },
  created(){
    
  },
  mounted(){
    var qrcode = new QRCode(this.$refs.qrcode, {      
      text: this.content,  // 二维码内容 url填后台给的链接    
      width:this.width,//控制二维码高度      
      height: this.height,//控制二维码宽度     
      colorDark: this.colorDark,//控制二维码前景色     
      colorLight: this.colorLight,//控制二维码背景色=         
      correctLevel: QRCode.CorrectLevel.H//设置二维码容错率   
    });  
  },
}