import * as func from "./func.js"
export default {
  name: 'SelectAddressPlus',
  props: {
    province: {type: String, default: ''},
    city: {type: String, default: ''},
    area:{type: String, default: ''},
    town:{type: String, default: ''},
    village:{type: String, default: ''},
    showTown: {type: Boolean, default: true},
    showVillage: {type: Boolean, default: true},
    showTitle: {type: Boolean, default: false},
  },
  data(){
    return {
      provinceList: [],
      cityList: [],
      areaList: [],
      townList: [],
      villageList: [],
      myProvince: '',
      myCity:'',
      myArea:'',
      myTown:'',
      myVillage:''
    }
  },
  watch:{
    province: function(newVal, oldVal){
      if(!newVal) {
        this.$refs.provinceRef.clearSingleSelect()
      }
    },
    city: function (newVal, oldVal){
      if(!newVal) {
        this.$refs.cityRef.clearSingleSelect()
      }
    },
    area: function (newVal, oldVal){
      if(!newVal) {
        this.$refs.areaRef.clearSingleSelect()
      }
    },
    town: function (newVal, oldVal){
      if(!newVal) {
        this.$refs.townRef.clearSingleSelect()
      }
    },
    village: function (newVal, oldVal){
      if(!newVal) {
        this.$refs.villageRef.clearSingleSelect()
      }
    },
  },
  async created(){
    this.provinceList = await this.getProvince() // 获取所有省列表 
    if(this.province) {
      this.myProvince = this.province
      this.cityList = await this.getCityList(this.province) // 获取当前省下面的市
    }
    if(this.city) {
      this.myCity = this.city
      this.areaList = await this.getAreaList(this.city) // 获取当前市下面的区
    }
    if(this.area) {
      this.myArea = this.area
      this.townList = await this.getTownList(this.area) // 获取当前区下面的镇
    }
    if(this.town) {
      this.myTown = this.town
      this.villageList = await this.getStreetList(this.town) // 获取当前镇下面的村
    }
    if(this.village) {
      this.myVillage = this.village
    }
  },
  methods:{
    ...func,
  },
  mounted(){
  },
}