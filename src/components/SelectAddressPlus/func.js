import { safeGet } from "@/common.js"
export const changeProvince = async function(e){
  this.$emit('setProvinceCode', e)
  if(!e) { return false }
  this.clearProvince()
  this.cityList = await this.getCityList(e) // 获取当前省下面的市
}
export const changeCity = async function(e){
  this.$emit('setCityCode', e)
  if(!e) { return false }
  this.clearCity()
  this.areaList = await this.getAreaList(e) // 获取当前市下面的区
}
export const changeArea = async function(e){
  this.$emit('setAreaCode', e)
  if(!e) { return false }
  this.clearArea()
  this.townList = await this.getTownList(e) // 获取当前区下面的镇
}
export const changeTown = async function(e){
  this.$emit('setTownCode', e)
  if(!e) { return false }
  this.clearTown()
  this.villageList = await this.getVillageList(e) // 获取当前镇下面的村
}
export const changeVillage = function(e){
  this.$emit('setVillageCode', e)
}
export const clearProvince = function(){
  this.$refs.cityRef.clearSingleSelect()
  this.$refs.areaRef.clearSingleSelect()
  this.$refs.townRef.clearSingleSelect()
  this.$refs.villageRef.clearSingleSelect()
  this.cityList=[]
  this.areaList = [];
  this.townList = [];
  this.villageList = [];
}
export const clearCity = function(){
  this.$refs.areaRef.clearSingleSelect()
  this.$refs.townRef.clearSingleSelect()
  this.$refs.villageRef.clearSingleSelect()
  this.areaList = [];
  this.townList = [];
  this.villageList = [];

}
export const clearArea = function(){
  this.$refs.townRef.clearSingleSelect()
  this.$refs.villageRef.clearSingleSelect()
  this.townList = [];
  this.villageList = [];
}
export const clearTown = function(){
  this.$refs.villageRef.clearSingleSelect()
  this.villageList = [];
}
export const getProvince = async function(){
  return this.$post('/address/regionalManagementList')
}
// 获取当前省下面的市列表
export const getCityList = function(province){
  if(!province) { return [] }
  const curItem = this.provinceList.find(item => item.name == province)
  if(!curItem) { return [] }
  return this.$post('/address/regionalManagementList', {
    regionId: curItem.code,
  }).then(res => safeGet(() => res[0].children, []))
}
// 获取当前市下面的区列表
export const getAreaList = function(city){
  if(!city) { return [] }
  const curItem = this.cityList.find(item => item.label == city)
  if(!curItem) { return [] }
  return this.$post('/address/regionalManagementList', {
    regionId: curItem.id,
  }).then(res => safeGet(() => res[0].children, []))
}
// 获取当前区下面的街道列表
export const getStreetList = function(area){
  if(!area) { return [] }
  const curItem = this.areaList.find(item => item.label == area)
  if(!curItem) { return [] }
  return this.$post('/address/regionalManagementList', {
    regionId: curItem.id,
  }).then(res => safeGet(() => res[0].children, []))
}