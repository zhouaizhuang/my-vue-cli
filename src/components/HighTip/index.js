import * as func from "./func.js"
export default {
  name: '',
  props: {
    value: {
      type: String,
      default: '',
    },
    limit:{
      type: Number,
      default: 50,
    }
  },
  components:{},
  data(){
    return {
      
    }
  },
  methods:{
    ...func,
  },
  created(){
    
  },
  mounted(){
    
  },
}