import * as func from "./func.js"
export default {
  name: '',
  props: {
    value: {
      type: String,
      default: '',
    },
    width: {
      type: [String, Number],
      default: 24,
    },
    height: {
      type: [String, Number],
      default: 24,
    },
    styleObj: {
      type: [Object],
      default: function (){
        return {}
      },
    }
  },
  components:{},
  data(){
    return {
      
    }
  },
  computed: {
    processVal: function(){
      if(this.width && this.height) {
        return this.value.replace(/(width=["]\d+["])/g, `width="${this.width}"`).replace(/(height=["]\d+["])/g, `height="${this.height}"`)
      } else {
        return this.value
      }
    }
  },
  methods:{
    ...func,
  },
  created(){
    
  },
  mounted(){
    
  },
}