/*
<HighModal
  :isShow="isShow"
  :title="'新增菜单'"
  @setIsShow="e=>isShow=e"
  @cancel="cancel"
  @confirm="confirm"
>
  <div class="fs14">
    <div>我是内容1</div>
    <div>我是内容2</div>
    <div>我是内容3</div>
    <div>我是内容4</div>
    <div>我是内容5</div>
    <div>我是内容6</div>
  </div>
</HighModal> 
*/
import * as func from "./func.js"
export default {
  name: '',
  props: {
    isShow: {
      type: Boolean,
      default: false, // 是否显示模态框 false为不显示  否则为显示
    },
    title: {
      type: String,
      default: '',
    },
  },
  components:{},
  data(){
    return {
      
    }
  },
  methods:{
    ...func,
  },
  computed:{
  },
  watch:{
  },
  created(){
    
  },
  mounted(){
    
  },
}